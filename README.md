## Instalar Dependências do Projeto
```sh
pip install -r requirements.txt
```
## Executar o código
```sh
python src/main.py assets/berlin52.tsp
```
## Executar dentro da pasta diagrams para criar o GIF
```sh
convert -delay 10 -loop 0 *.png animation.gif
```